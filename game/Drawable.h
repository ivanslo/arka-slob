#ifndef DRAWABLE_H
#define DRAWABLE_H


class Drawable
{
    public:
        Drawable();
        virtual ~Drawable();
        virtual void draw() = 0;
        bool isVisible();
    protected:
        bool visible;
    private:
};

#endif // DRAWABLE_H
