#ifndef BALL_H
#define BALL_H

#include <gl/gl.h>
#include <gl/glu.h>

#include "Drawable.h"
#include "BasicElement.h"

class Ball : public BasicElement
{
    public:
        Ball(int id);
        Ball(int id, float x, float y , float z);
        virtual ~Ball();

        /* from BasicElement */
        float leftSide();
        float rightSide();
        float topSide();
        float bottonSide();

        void draw();
        void move();


        /* setters */
        void setRadius(float a) { radius = (a>0) ? a : 1; };

        void setDespX(float a) { dX = a;};
        void setDespY(float a) { dY = a;};
        void setDirX (float a) { dirX = a;};
        void setDirY (float a) { dirY = a;};
        void setX    (float a) { x = a; };
        void setY    (float a) { y = a; };

        void changeDirX() { dirX *= -1;};
        void changeDirY() { dirY *= -1;};

    protected:
    private:
        GLUquadricObj *ball;
        GLfloat     radius;
        float   dX,
                dY,
                dirX,
                dirY;
};

#endif // BALL_H
