#include "Ball.h"

#include <iostream>
Ball::Ball(int id):BasicElement(id)
{
    ball = gluNewQuadric();
    radius = 1.0f;
}

Ball::Ball(int id, float x, float y, float z) : BasicElement(id, x, y, z)
{
    ball = gluNewQuadric();
    radius = 1.0f;
}

Ball::~Ball()
{
}

void Ball::move()
{
    x += dX * dirX;
    y += dY * dirY;
    needReDraw = true;
}

void Ball::draw()
{
    glPushMatrix();
        glColor3f(1, 1, 1);
        gluSphere(ball, radius, 32, 32);  // arbitrary resolution of 32
    glPopMatrix();
}

float Ball::leftSide()
{
    return x - radius;
}

float Ball::rightSide()
{
    return x + radius;
}

float Ball::topSide()
{
    return y + radius;
}

float Ball::bottonSide()
{
    return y - radius;
}
