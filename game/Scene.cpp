#include "Scene.h"

#include <iostream>
Scene::Scene()
{
    for( int i = 0; i < ELEMENTS_LIMIT; i++ )
        gameElements[i] = NULL;

    for( int i = 0; i < BRICKS_LIMIT; i++ )
       bricks[i] = NULL;

    nextId = 0;
    elementsCount = 0;

    playing = false;



}

Scene::~Scene()
{
    cleanScene();
    delete border1;
    delete border2;
    delete border3;
    delete ball;
    delete raquette;
}

void Scene::setDimension(float width, float height)
{
    this->dim_width = width;
    this->dim_height = height;
}

void Scene::addGameElement( BasicElement *element)
{
    gameElements[elementsCount] = element;
    elementsCount++;
}

void Scene::init()
{
        /* ball */
    ball = new Ball( 10004 , 0, 2, 0);
    ball->setRadius( 0.3f );


    /* borders */
    border1 = new Bar(10001, -dim_width/2, 0, 0, VERTICAL);
    border1->setRadius(0.2);
    border1->setLenght(dim_height);

    border2 = new Bar(10002, -dim_width/2, dim_height, 0, HORIZONTAL);
    border2->setRadius(0.2);
    border2->setLenght(dim_width);

    border3 = new Bar(10003, dim_width/2, 0, 0, VERTICAL);
    border3->setRadius(0.2);
    border3->setLenght(dim_height);
}
int Scene::getNextId()
{
    nextId = nextId+1 % 10000; /* limit */
    return nextId;
}

void Scene::cleanScene()
{
    /* element number 0 is raquette */
    for( int i = 0; i < ELEMENTS_LIMIT; i++ )
    {
       if( dynamic_cast<Brick*> (gameElements[i]) )
       {
           delete gameElements[i];
       }
       gameElements[i] = NULL;

    }
    elementsCount = 0;
    for( int i = 0; i < BRICKS_LIMIT; i++ )
    {
       bricks[i] = 0;
    }
    nextId = 0;
    glDeleteLists(1, nextId);
    playing = false;
}

void Scene::initLevel( int level )
{
    ball->setDespX( 0.18f );
    ball->setDespY( 0.13f );
    ball->setDirX( 1 );
    ball->setDirY( 1 );
    ball->setX( 0 );
    ball->setY( 5 );
    addGameElement( border1 );
    addGameElement( border2 );
    addGameElement( border3 );
    addGameElement( raquette );
    addGameElement( ball );

    /* set bricks level */
    if( level == 1 )    /* *** LEVEL 1 *** */
    {
        bricks = {  2,  0,  2,  0,  2,  2,  2,
                    1,  0,  1,  0,  1,  0,  1,
                    2,  0,  2,  0,  2,  2,  0,
                    3,  3,  3,  0,  3,  0,  3,
                    0,  2,  0,  0,  2,  0,  2,
        };
    }
    if( level == 2 )    /* *** LEVEL 2 *** */
    {
        bricks = {  2,  0,  2,  0,  2,  0,  2,
                    0,  3,  0,  3,  0,  3,  0,
                    0,  0,  0,  0,  0,  0,  0,
                    1,  1,  1,  1,  1,  1,  1,
                    0,  0,  0,  0,  0,  0,  0,
        };
    }
    if( level == 3 )    /* *** LEVEL 3 *** */
    {
        bricks = {  1,  2,  3,  1,  2,  3,  1,
                    2,  3,  1,  2,  3,  1,  2,
                    3,  1,  2,  3,  1,  2,  3,
                    1,  2,  3,  1,  2,  3,  1,
                    2,  3,  1,  2,  3,  1,  2,
        };
    }

    float _x, _y, _z;
    Brick *b;

    lifesBricksPlaying = 0;
    for( int i = 0; i < BRICKS_LIMIT; i++ ){
        if( bricks[i] ){
            /* harcoded for 20x20 scene , and 7 x 5 bricks */
            _x = -9.8f + 1.4 + 2.8f * (i%7);
            _y = 18 - 0.5f - ((0.5 + 1.0f) * (i/7));
            _z = 0;
            b = new Brick(getNextId(), _x, _y, _z, 2.0f, 0.5f, 0.4f);
            b->setLives( bricks[i] );

            addGameElement( b );
            lifesBricksPlaying += bricks[i];
        }
    }

    levelPlaying = level;
    is_won = false;
    is_loss = false;
    playing = false;
}

void Scene::drawBackground()
{
    glBindTexture(GL_TEXTURE_2D, textureBack);
    glPushMatrix();

    glTranslatef( 0, dim_height/2, -0.2 );
    glScalef( dim_width/2 , dim_height/2 , 1 );

    glBegin(GL_QUADS);
        glColor3f(1,1,1);;
        glNormal3f(0,0,1);
        glTexCoord2d(0.0,0.0); glVertex3f(-1.0, -1.0, 0);
        glTexCoord2d(1.0,0.0); glVertex3f(1.0, -1.0, 0);
        glTexCoord2d(1.0,1.0); glVertex3f(1.0, 1.0, 0);
        glTexCoord2d(0.0,1.0); glVertex3f(-1.0, 1.0, 0);
    glEnd();

    glPopMatrix();
}

void Scene::draw()
{
    glClearColor( 0.3f, 0.3f , 0.3f ,0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    drawBackground();

    if( playing ){
        ball->move();
        if( ball->topSide() < -0.6)
            this->loss(); /* ball is out of limit */
    }


    int collision;
    for(int i = 0; i < elementsCount; i++ )
    {
        if(gameElements[i] == NULL ) continue;

        collision = ball->isCollisioningWith(gameElements[i]);

        if( collision != COL_NONE )
        {
            if( collision == COL_TOP || collision == COL_BOTTON )
            {
                ball->changeDirY();
            }
            if( collision == COL_LEFT || collision == COL_RIGHT )
            {
                ball->changeDirX();
            }

            if( dynamic_cast<Brick*> (gameElements[i]) ) /* collision is with a Brick */
            {
                (dynamic_cast<Brick*> (gameElements[i]))->punch();
                lifesBricksPlaying -= 1;
                if( lifesBricksPlaying <= 0 )
                    this->win(); /* every bricks broken */
            }
        }
    }

    for(int i = 0; i < elementsCount; i++ )
    {
        if(gameElements[i] == NULL ) continue;
            gameElements[i]->render();
    }
    ball->render();

}

void Scene::loss()
{
    playing = false;
    is_won = false;
    is_loss = true;
}

void Scene::win()
{
    playing = false;
    is_won = true;
    is_loss = false;
    //nextLevel = levelPlaying;
}
void Scene::play()
{
    if( is_loss ){
        cleanScene();
        initLevel( levelPlaying );
        return;
    }
    if( is_won ) {
        cleanScene();
        if( levelPlaying < 3)
            initLevel( levelPlaying + 1 );
        else
            initLevel( 1 );

        return;
    }

    playing = true;
}

