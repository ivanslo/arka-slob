#ifndef RAQUETTE_H
#define RAQUETTE_H

#include "Brick.h"


class Raquette : public BasicElement
{
    public:
        Raquette(int id);
        Raquette(int id, GLfloat x, GLfloat y, GLfloat z, GLfloat lenght, GLfloat width, GLfloat height);
        virtual ~Raquette();

        void moveLeft();
        void moveRight();
        void moveUp();
        void moveDown();

        /* from BasicElement */
        float leftSide();
        float rightSide();
        float topSide();
        float bottonSide();

        void draw();

        /* setters */
        void setLenght( float a ){ lenght = (a>0) ? a/2 : 1; };
        void setWidth ( float a ){ width  = (a>0) ? a/2 : 1; };
        void setHeight( float a ){ height = (a>0) ? a/2 : 1; };
        void setDespX  ( float a ){ dX = a; };
        void setDespY  ( float a ){ dY = a; };

        void setLimitsOfMovement( float botton, float top, float left, float right );

    protected:
    private:
        GLfloat lenght,
                width,
                height;
        float   limit_left,
                limit_right,
                limit_top,
                limit_botton;
        float   dX,
                dY;
};

#endif // RAQUETTE_H
