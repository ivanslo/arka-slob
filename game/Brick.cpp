#include "Brick.h"
#include "../helpers/OpenGLHelpers.h"

Brick::Brick(int id): BasicElement(id)
{
    setWidth ( 1 );
    setLenght( 1 );
    setHeight( 1 );

    setLives( 1 );
}

Brick::Brick(int, GLfloat x, GLfloat y, GLfloat z): BasicElement(id, x, y, z)
{
    setWidth ( 1 );
    setLenght( 1 );
    setHeight( 1 );

    setLives( 1 );
}


Brick::Brick(int id, GLfloat x, GLfloat y, GLfloat z,
              GLfloat lenght, GLfloat width, GLfloat height): BasicElement(id, x, y, z)
{
    setWidth ( width  );
    setLenght( lenght );
    setHeight( height );

    setLives( 1 );
}

Brick::~Brick()
{
}

void Brick::punch()
{
    lives--;

    if( lives <= 0 ){
        this->setVisible( false );
    }
    needReDraw = true;
}

void Brick::draw()
{
    glScalef(this->lenght, this->width, this->height);

    if( lives == 3 ) glBindTexture(GL_TEXTURE_2D, texture[2] );
    if( lives == 2 ) glBindTexture(GL_TEXTURE_2D, texture[1] );
    if( lives == 1 ) glBindTexture(GL_TEXTURE_2D, texture[0] );

    glPushMatrix();

    glBegin(GL_QUADS);
        glColor3f(1,1,1);

        /*front face */
        glNormal3f(0.0, 0.0, 1.0);
        glTexCoord2d(0.0,0.0); glVertex3f(-1.0, -1.0, 1.0);
        glTexCoord2d(1.0,0.0); glVertex3f(1.0, -1.0, 1.0);
        glTexCoord2d(1.0,1.0); glVertex3f(1.0, 1.0, 1.0);
        glTexCoord2d(0.0,1.0); glVertex3f(-1.0, 1.0, 1.0);
        /* back face */
        glNormal3f(0.0, 0.0, -1.0);
        glTexCoord2d(0.0,0.0); glVertex3f(-1.0, 1.0, -1.0);
        glTexCoord2d(0.0,1.0); glVertex3f(1.0, 1.0, -1.0);
        glTexCoord2d(1.0,1.0); glVertex3f(1.0, -1.0, -1.0);
        glTexCoord2d(1.0,0.0); glVertex3f(-1.0, -1.0, -1.0);
        /* top side face */
        glNormal3f(0.0, 1.0, 0.0);
        glTexCoord2d(0.0,0.0); glVertex3f(-1.0, 1.0, 1.0);
        glTexCoord2d(0.0,1.0); glVertex3f(1.0, 1.0, 1.0);
        glTexCoord2d(1.0,1.0); glVertex3f(1.0, 1.0, -1.0);
        glTexCoord2d(1.0,0.0); glVertex3f(-1.0, 1.0, -1.0);
        /* bottom side face */
        glNormal3f(0.0, -1.0, 0.0);
        glTexCoord2d(0.0,1.0); glVertex3f(-1.0, -1.0, 1.0);
        glTexCoord2d(1.0,1.0); glVertex3f(1.0, -1.0, 1.0);
        glTexCoord2d(1.0,0.0); glVertex3f(1.0, -1.0, -1.0);
        glTexCoord2d(0.0,0.0); glVertex3f(-1.0, -1.0, -1.0);
        /* left side face*/
        glNormal3f(-1.0, 0.0, 0.0);
        glTexCoord2d(0.0,0.0); glVertex3f(-1.0, 1.0, -1.0);
        glTexCoord2d(1.0,0.0); glVertex3f(-1.0, -1.0, -1.0);
        glTexCoord2d(1.0,1.0); glVertex3f(-1.0, -1.0, 1.0);
        glTexCoord2d(0.0,1.0); glVertex3f(-1.0, 1.0, 1.0);
        /* right side face */
        glNormal3f(1.0, 0.0, 0.0);
        glTexCoord2d(0.0,0.0); glVertex3f(1.0, 1.0, -1.0);
        glTexCoord2d(0.0,0.0);  glVertex3f(1.0, -1.0, -1.0);
        glTexCoord2d(0.0,0.0); glVertex3f(1.0, -1.0, 1.0);
        glTexCoord2d(0.0,0.0); glVertex3f(1.0, 1.0, 1.0);
    glEnd();
    glPopMatrix();
}

void Brick::setLives( int a )
{
    lives = a;
    if( lives <= 0 ) lives = 0;
    if( lives >  3 ) lives = 3;
}

float Brick::leftSide()
{
    return x - lenght;
}
float Brick::rightSide()
{
    return x + lenght;
}
float Brick::topSide()
{
    return y + width;
}
float Brick::bottonSide()
{
    return y - width;
}
