#include "BasicElement.h"

BasicElement::BasicElement(int id)
{
    this->id = id;
    this->x = this->y = this->z = 0;

    visible = true;
    needReDraw = true;
    this->color_r = this->color_g = this->color_b = 1.0f;
}

BasicElement::BasicElement(int id, float x, float y, float z)
{
    this->id = id;
    this->x = x;
    this->y = y;
    this->z = z;

    visible = true;
    needReDraw = true;
}

BasicElement::~BasicElement()
{
}

void BasicElement::render()
{
    if( !this->isVisible() )
        return;

    if( !needReDraw )
    {
        glCallList( this->id );
        return;
    }

    glNewList(id, GL_COMPILE_AND_EXECUTE);
    glPushMatrix();
        glTranslatef(this->x, this->y, this->z);
        draw(); /* specific */
    glPopMatrix();
    glEndList();

    needReDraw = false;
}

void BasicElement::setColor(float r, float g, float b)
{
    color_r = r;
    color_g = g;
    color_b = b;
}

void BasicElement::move()
{
    // the most of elements do not have movement
}


/*
 * The method of Collision Detection is a little 'rustic', because it sees every
 * BasicElement as a rectangle with four sides (right, left, top and botton).
 * Only made for 2D detections.
 *
 * The return value indicates in which side of 'this' is produced the collision
 * according to Collision enum. Then, is possible to take the right decision.
 *
*/
int BasicElement::isCollisioningWith( BasicElement* e)
{
    if( !this->isVisible() || ! e->isVisible() )
    {
        return COL_NONE;
    }

    int collision = COL_NONE;

    /* previous position */
    float prev_rightSide    = this->rightSide();
    float prev_leftSide     = this->leftSide();
    float prev_topSide      = this->topSide();
    float prev_bottontSide  = this->bottonSide();
    float prev_x            = x;
    float prev_y            = y;

    /* simulate in one step forward */
    this->move();


    if( this->rightSide() - e->leftSide() >= 0 && this->rightSide() - e->rightSide() <= 0)
    {
        if( this->topSide() - e->bottonSide() >= 0 && this->topSide() - e->topSide() <= 0)
        {
            if( prev_rightSide - e->leftSide() < 0 )
            {
                collision = COL_RIGHT;
            }

            if( prev_topSide - e->bottonSide() < 0 )
            {
                collision = COL_TOP;
            }
        }
        else if( this->bottonSide() - e->topSide() <= 0 && this->bottonSide() - e->bottonSide() >= 0)
        {
            if( prev_rightSide - e->leftSide() < 0)
            {
                collision = COL_TOP;
            }

            if( prev_bottontSide - e->topSide() > 0 )
            {
                collision = COL_BOTTON;
            }
        }
    }
    else if( this->leftSide() - e->rightSide() <= 0 && this->leftSide() - e->leftSide() >= 0)
    {
        if( this->topSide() - e->bottonSide() >= 0 && this->topSide() - e->topSide() <= 0)
        {
            if( prev_leftSide - e->rightSide() > 0 )
            {
                collision = COL_LEFT;
            }
            if( prev_topSide - e->bottonSide() < 0 )
            {
                collision = COL_TOP;
            }
        }
        else if( this->bottonSide() - e->topSide() <= 0 && this->bottonSide() - e->bottonSide() >= 0)
        {
            if( prev_leftSide - e->rightSide() > 0)
            {
                collision = COL_LEFT;
            }
            if( prev_bottontSide - e->topSide() > 0 )
            {
                collision = COL_BOTTON;
            }
        }
    }

    /* restore position */
    x = prev_x;
    y = prev_y;

    return collision;
}


