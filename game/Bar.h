#ifndef BAR_H
#define BAR_H


#include <gl/gl.h>
#include <gl/glu.h>
#include "BasicElement.h"

class Bar : public BasicElement
{
    public:
        Bar(int id, float, float, float, int orientation );
        virtual ~Bar();

        /* from BasicElement */
        float leftSide();
        float rightSide();
        float topSide();
        float bottonSide();

        void draw();

        /* setters */
        void setRadius(float a) { radius = (a>0) ? a : 1; };
        void setLenght(float a) { lenght = (a>0) ? a : 1; };

        void setOrientation(int a)  { orientation = a; };

    protected:
    private:
        GLUquadricObj *bar;
        float   lenght,
                radius;
        int     orientation;
};

#endif // BARS_H
