#ifndef SCENE_H
#define SCENE_H

#include <gl/gl.h>
#include <gl/glu.h>

#include "Brick.h"
#include "Bar.h"
#include "Ball.h"
#include "Raquette.h"
#include "BasicElement.h"


#include "../globalVars.h"


class Scene
{
    static const int BRICKS_LIMIT = 35;
    static const int ELEMENTS_LIMIT = 200;

    public:
        Scene();
        virtual ~Scene();
        void cleanScene();

        void draw();
        void init();
        void initLevel(int level);

        void addRaquette( Raquette * r) { raquette = r; };
        void addGameElement( BasicElement *element);

        void play();
        void pause(){ playing = false; };

        void win();
        void loss();

        void setDimension( float width, float height );
    protected:

    private:
        int nextId;
        int getNextId();

        float   dim_width,
                dim_height;

        void drawBackground();

        int bricks[BRICKS_LIMIT];

        Bar *border1, *border2, *border3, *border4;
        Ball * ball;
        Raquette *raquette;

        bool    playing,
                is_won,
                is_loss;
        int levelPlaying;

        BasicElement * gameElements[ELEMENTS_LIMIT];
        int elementsCount;
        int lifesBricksPlaying;

//        void checkWin();

};

#endif // SCENE_H
