#ifndef BRICK_H
#define BRICK_H

#include <gl/gl.h>
#include <gl/glu.h>
#include "../globalVars.h"
#include "BasicElement.h"

class Brick : public BasicElement
{
    public:
        Brick(int);
        Brick(int, GLfloat x, GLfloat y, GLfloat z);
        Brick(int, GLfloat x, GLfloat y, GLfloat z, GLfloat width, GLfloat height, GLfloat lenght);
        virtual ~Brick();

        virtual void punch();

        /* from BasicElement */
        float leftSide();
        float rightSide();
        float topSide();
        float bottonSide();

        void draw();

        /* setters */
        void setLenght( float a ){ lenght = (a>0) ? a/2 : 1; };
        void setWidth ( float a ){ width  = (a>0) ? a/2 : 1; };
        void setHeight( float a ){ height = (a>0) ? a/2 : 1; };
        void setLives( int a );

    protected:
    private:
        GLfloat lenght,
                width,
                height;
        int lives;

};

#endif // BRICK_H
