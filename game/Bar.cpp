#include "Bar.h"

Bar::Bar(int id, float x, float y, float z, int orientation ):BasicElement(id, x, y, z)
{
    bar = gluNewQuadric();
    setLenght(  2  );
    setRadius( 0.5 );
    setOrientation( orientation );
}
Bar::~Bar()
{
}
void Bar::draw()
{
   glPushMatrix();
        if( orientation == VERTICAL )
        {
            glRotatef(-90.0f, 1, 0, 0);
        }
        if ( orientation == HORIZONTAL )
        {
            glRotatef(90.0f, 0, 1, 0);
        }
        glColor3f(color_r, color_g, color_b );
        gluCylinder(bar, radius, radius, lenght, 32, 32 ); // arbitrary resolution
   glPopMatrix();
}

float Bar::leftSide()
{
    if( orientation == VERTICAL )
       return x - radius;
    return x;
}

float Bar::rightSide()
{
    if( orientation == VERTICAL )
        return x + radius;
    return x + lenght;
}

float Bar::topSide()
{
    if( orientation == VERTICAL )
        return y + lenght;
    return y + radius;
}

float Bar::bottonSide()
{
    if( orientation == VERTICAL )
        return y;
    return y - radius;
}
