#include "Raquette.h"

#include <iostream>

Raquette::Raquette(int id): BasicElement(id)
{
    setWidth ( 1 );
    setLenght( 4 );
    setHeight( 1 );
}

Raquette::Raquette(int id, GLfloat x, GLfloat y, GLfloat z, GLfloat lenght, GLfloat width, GLfloat height)
        :BasicElement (id, x, y, z)
{
    setWidth ( width  );
    setLenght( lenght );
    setHeight( height );

    int bigNum = 100;
    setLimitsOfMovement(-bigNum, bigNum, -bigNum, bigNum);
}

Raquette::~Raquette()
{
}

void Raquette::setLimitsOfMovement( float botton, float top, float left, float right )
{
    limit_botton = botton;
    limit_top    = top;
    limit_left   = left;
    limit_right  = right;
}

void Raquette::moveLeft()
{
     if( leftSide() > limit_left )
    {
        x -= dX;
        needReDraw = true;
    }
}

void Raquette::moveRight()
{
    if( rightSide() < limit_right )
    {
        x += dX;
        needReDraw = true;
    }
}

void Raquette::moveUp()
{
    if( topSide() < limit_top )
    {
        y += dY;
        needReDraw = true;
    }
}

void Raquette::moveDown()
{
    if( bottonSide() > limit_botton )
    {
        y -= dY;
        needReDraw = true;
    }
}

void Raquette::draw()
{
    glScalef(this->lenght, this->width, this->height);
    glBindTexture(GL_TEXTURE_2D, textureRaq );

    glPushMatrix();

    glBegin(GL_QUADS);
        glColor3f(1,1,1);

        /*front face */
        glNormal3f(0.0, 0.0, 1.0);
        glTexCoord2d(0.0,0.0); glVertex3f(-1.0, -1.0, 1.0);
        glTexCoord2d(1.0,0.0); glVertex3f(1.0, -1.0, 1.0);
        glTexCoord2d(1.0,1.0); glVertex3f(1.0, 1.0, 1.0);
        glTexCoord2d(0.0,1.0); glVertex3f(-1.0, 1.0, 1.0);
        /* back face */
        glNormal3f(0.0, 0.0, -1.0);
        glTexCoord2d(0.0,0.0); glVertex3f(-1.0, 1.0, -1.0);
        glTexCoord2d(0.0,1.0); glVertex3f(1.0, 1.0, -1.0);
        glTexCoord2d(1.0,1.0); glVertex3f(1.0, -1.0, -1.0);
        glTexCoord2d(1.0,0.0); glVertex3f(-1.0, -1.0, -1.0);
        /* top side face */
        glNormal3f(0.0, 1.0, 0.0);
        glTexCoord2d(0.0,0.0); glVertex3f(-1.0, 1.0, 1.0);
        glTexCoord2d(0.0,1.0); glVertex3f(1.0, 1.0, 1.0);
        glTexCoord2d(1.0,1.0); glVertex3f(1.0, 1.0, -1.0);
        glTexCoord2d(1.0,0.0); glVertex3f(-1.0, 1.0, -1.0);
        /* bottom side face */
        glNormal3f(0.0, -1.0, 0.0);
        glTexCoord2d(0.0,1.0); glVertex3f(-1.0, -1.0, 1.0);
        glTexCoord2d(1.0,1.0); glVertex3f(1.0, -1.0, 1.0);
        glTexCoord2d(1.0,0.0); glVertex3f(1.0, -1.0, -1.0);
        glTexCoord2d(0.0,0.0); glVertex3f(-1.0, -1.0, -1.0);
        /* left side face*/
        glNormal3f(-1.0, 0.0, 0.0);
        glTexCoord2d(0.0,0.0); glVertex3f(-1.0, 1.0, -1.0);
        glTexCoord2d(1.0,0.0); glVertex3f(-1.0, -1.0, -1.0);
        glTexCoord2d(1.0,1.0); glVertex3f(-1.0, -1.0, 1.0);
        glTexCoord2d(0.0,1.0); glVertex3f(-1.0, 1.0, 1.0);
        /* right side face */
        glNormal3f(1.0, 0.0, 0.0);
        glTexCoord2d(0.0,0.0); glVertex3f(1.0, 1.0, -1.0);
        glTexCoord2d(0.0,0.0);  glVertex3f(1.0, -1.0, -1.0);
        glTexCoord2d(0.0,0.0); glVertex3f(1.0, -1.0, 1.0);
        glTexCoord2d(0.0,0.0); glVertex3f(1.0, 1.0, 1.0);
    glEnd();
    glPopMatrix();
}

float Raquette::leftSide()
{
    return x - lenght;
}

float Raquette::rightSide()
{
    return x + lenght;
}

float Raquette::topSide()
{
    return y + width;
}

float Raquette::bottonSide()
{
    return y - width;
}
