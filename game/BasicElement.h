#ifndef BASICELEMENT_H
#define BASICELEMENT_H

#include "../globalVars.h"

class BasicElement
{
    public:
        BasicElement(int id);
        BasicElement(int id, float x, float y, float z);

        virtual ~BasicElement();

        virtual void move(); // collision
        int isCollisioningWith( BasicElement* );   // basic
        void render();

        virtual float leftSide()    = 0;
        virtual float rightSide()   = 0;
        virtual float topSide()     = 0;
        virtual float bottonSide()  = 0;


        virtual void draw() = 0;

        void setColor(float r, float g, float b);   //RGB form

        bool isVisible()        { return visible;   };
        void setVisible(bool v) { visible = v;      };

    protected:
        int id;
        // position
        float x, y, z;

        // visibility, color
        bool visible;
        bool needReDraw;
        float color_r, color_g, color_b;
//        s
    private:

};

#endif // BASICELEMENT_H
