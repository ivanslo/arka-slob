/* **************************************************************************** */
/*      Author: Ivan Slobodiuk
           To : myVR Software - Oslo, Norway
       Project: ArkaSlob
          Date: 03/12/2012

    This is an example of OpenGL running in Windows.

                                CONTROLS
                        =====================
                        SPACE : play / start
                            P : pause
        LEFT, RIGHT, UP, DOWN : Move the Raquette
                         A, Z : Move the point of view height
                          ESC : Close the windows

*/


#include <windows.h>
#include <iostream>
#include <gl/gl.h>
#include <gl/glu.h>

#include "helpers/WindowsHelpers.h"
#include "helpers/OpenGLHelpers.h"
#include "globalVars.h"
#include "game/Brick.h"
#include "game/Scene.h"
#include "game/Raquette.h"
#include "game/Drawable.h"


bool keys[256];

GLfloat zPov = 13.2;

GLuint	texture[3];
GLuint  textureBack;
GLuint  textureRaq;


int WINAPI WinMain (HINSTANCE hThisInstance, HINSTANCE hPrevInstance,
                     LPSTR lpszArgument, int nCmdShow)
{
    HWND hwnd;              /* Window Handler */
    MSG msg;                /* Messages for Application */
    HDC hDC;                /* Device Context */
    HGLRC hRC;              /* Rendering Context */
    bool quitFlag = false;

    hwnd = createWindows( hThisInstance );

    ShowWindow( hwnd, nCmdShow );

    enableOpenGL(hwnd, &hDC, &hRC);

    configureGL();

    loadTextures();


    /* load main components */
    Scene *scene = new Scene();
        scene->setDimension( 20.0f, 20.0f);
        scene->init();
    Raquette *raquette = new Raquette(10000, 0,0,0, 3.5, 0.5f, 0.4f);
        raquette->setDespX( 0.24f );
        raquette->setDespY( 0.10f );
        raquette->setLimitsOfMovement(0, 3, -9.7, 9.7);
    scene->addRaquette( raquette );
    scene->initLevel( 1 );

    /* Main Loop */
    while (!quitFlag)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
            {
                quitFlag = TRUE;
            }
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            /* Key Control */
                /* point of view */
            if( keys['a'] || keys['A'])
            { // Up View
                zPov = (zPov >= 25.0) ? 25.0 : (zPov + 0.2f);
                configureGL();
            }
            if( keys['z'] || keys['Z'])
            { // Down View
                zPov = (zPov <=  3.6) ?  3.6 : (zPov - 0.2f);
                configureGL();
            }
                /* raquette movement */
            if( keys[VK_UP]   ) raquette->moveUp();
			if( keys[VK_DOWN] ) raquette->moveDown();
			if( keys[VK_RIGHT]) raquette->moveRight();
            if( keys[VK_LEFT] )	raquette->moveLeft();

                /* game control and quit */
			if( keys[VK_SPACE]          ) scene->play();
			if( keys['p'] || keys['P']  ) scene->pause();
			if( keys[VK_ESCAPE]         ) quitFlag = TRUE;


            glPushMatrix();
               scene->draw();
            glPopMatrix();

            SwapBuffers(hDC);

            Sleep(10); /* refresh rate */
        }
    }

    disableOpenGL(hwnd, hDC, hRC);

    DestroyWindow(hwnd);

    return msg.wParam;
}

