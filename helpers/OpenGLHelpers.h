#ifndef OPENGLHELPERS_H
#define OPENGLHELPERS_H

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include "../globalVars.h"

void enableOpenGL( HWND , HDC*, HGLRC* );
void disableOpenGL(HWND , HDC,  HGLRC );
void configureGL();
void loadTextures();
GLuint loadTextureRAW( const char * filename, int wrap, int width, int height );

#endif // OPENGLHELPERS_H
