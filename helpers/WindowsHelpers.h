#ifndef WINDOWSHELPERS_H
#define WINDOWSHELPERS_H

#include <windows.h>
#include "../globalVars.h"

LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);

HWND createWindows( HINSTANCE );

#endif // WINDOWSHELPERS_H
