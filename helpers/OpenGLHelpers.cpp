#include "OpenGLHelpers.h"
#include <stdio.h>
#include <iostream>


void enableOpenGL(HWND hwnd, HDC* hDC, HGLRC* hRC)
{
    PIXELFORMATDESCRIPTOR pfd;

    int iFormat;

    *hDC = GetDC(hwnd);

    ZeroMemory(&pfd, sizeof(pfd));

    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cDepthBits = 16;
    pfd.iLayerType = PFD_MAIN_PLANE;

    iFormat = ChoosePixelFormat(*hDC, &pfd);

    SetPixelFormat(*hDC, iFormat, &pfd);

    *hRC = wglCreateContext(*hDC);

    wglMakeCurrent(*hDC, *hRC);
}

void disableOpenGL (HWND hwnd, HDC hDC, HGLRC hRC)
{
    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(hRC);
    ReleaseDC(hwnd, hDC);
}



void configureGL()
{
    glEnable(GL_TEXTURE_2D);
	glShadeModel(GL_SMOOTH);
	glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHT0);
	glEnable(GL_NORMALIZE);

    // Projection
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0f, (GLfloat) 1024 / (GLfloat) 768, 1.0f, 200.0f );
    glViewport(0, 0, 1024, 768);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0 , -5, zPov, 0, 5, 0, 0, 0, 1); // (x,y,z) from  - (x,y,z) to - (x,y,z) Normal Up

    // Light Configuration
    GLfloat LightAmbient []=	{ 0.5f, 0.5f, 0.5f, 0.2f };
    GLfloat LightDiffuse []=	{ 0.7f, 0.6f, 1.0f, 0.4f };
    GLfloat LightPosition[]=    { 5.0, 10.0, 40.0, 0.0 };
	glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuse);
	glLightfv(GL_LIGHT1, GL_POSITION,LightPosition);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHTING);
}

void loadTextures()
{
    textureRaq  = loadTextureRAW("data/wood2.raw"   , true, 256, 256);
    textureBack = loadTextureRAW("data/wood1.raw"   , true, 1024, 1024);
    texture[0]  = loadTextureRAW("data/1live_.raw"  , true, 256, 64);
    texture[1]  = loadTextureRAW("data/2lives_.raw" , true, 256, 64);
    texture[2]  = loadTextureRAW("data/3lives_.raw" , true, 256, 64);

}

GLuint loadTextureRAW( const char * filename, int wrap , int width, int height)
{
    /* Method based in the next article: http://www.nullterminator.net/gltexture.html */
    GLuint texture;
    BYTE * data;
    FILE * file;

    file = fopen( filename, "rb" );
    if ( file == NULL )
    {
        std::cout << "Error Opening Texture "<<filename<<std::endl;
        return 0;
    }

    data =  (BYTE* )malloc( width * height * 3 );

    fread( data, width * height * 3, 1, file );
    fclose( file );

    glGenTextures( 1, &texture );
    glBindTexture( GL_TEXTURE_2D, texture );
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap ? GL_REPEAT : GL_CLAMP );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap ? GL_REPEAT : GL_CLAMP );

    gluBuild2DMipmaps( GL_TEXTURE_2D, 3, width, height, GL_RGB, GL_UNSIGNED_BYTE, data );

    free( data );

    return texture;
}

