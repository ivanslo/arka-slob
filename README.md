	   Author: Ivan Slobodiuk
          To : myVR Software - Oslo, Norway
      Project: ArkaSlob
         Date: 03/12/2012

    This is an example of OpenGL running in Windows.

                         CONTROLS
                  =====================
                    SPACE : play / start
                        P : pause
    LEFT, RIGHT, UP, DOWN : Move the Raquette
                     A, Z : Move the point of view height
                      ESC : Close the windows