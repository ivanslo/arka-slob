#ifndef GLOBALVARS_H_INCLUDED
#define GLOBALVARS_H_INCLUDED

#include <gl/gl.h>

extern bool keys[256]; /* KeyBoard Control */

/* textures for elements */
extern GLuint texture[3];
extern GLuint textureBack;
extern GLuint textureRaq;

/* point of view */
extern GLfloat  zPov;

/* enums */
enum CollisionIn
{
    COL_NONE, COL_LEFT, COL_RIGHT, COL_TOP, COL_BOTTON
};
enum Orientation {
    VERTICAL,
    HORIZONTAL
};
#endif // GLOBALVARS_H_INCLUDED
